import * as React from "react";
import {MouseEventHandler} from 'react';
import { connect } from 'react-redux';
import { Dispatch, bindActionCreators } from 'redux';

import * as actions from "../../store/actions";
import { RootState } from "../../reducers";
import { Action } from './../../shared/models/interface';

interface OwnProps {}

export interface ILocationSelectProps {
    fwd?: MouseEventHandler;
    back?: MouseEventHandler;
    visible: boolean;
    fEnable: boolean;
    bEnable: boolean
}

// Render location back-forward controls
const LocationSelect: React.SFC<ILocationSelectProps> = ({back, fwd, visible, bEnable, fEnable}: ILocationSelectProps) => {
    if( !visible ) {
        return null;
    }
    return (
        <div className = "select-location text-light">
            <h6>Select location</h6>
            <button onClick={back} disabled={!bEnable}>{'< Bck'}</button>
            <button onClick={fwd} disabled={!fEnable}>{'Fwd >'}</button>
        </div>
    );
}

const mapStateToProps = (state: RootState): ILocationSelectProps => ({
        visible: (state.app.locationLen > 1),
        bEnable: state.app.currentLocation < state.app.locationLen - 1,
        fEnable: state.app.currentLocation > 0
    });

const mapDispatchToProps = (dispatch: Dispatch<Action>, props: OwnProps) => bindActionCreators({
        fwd: () => actions.locationFFWDAction(),
        back: () => actions.locationBACKAction(),
    }, dispatch);
  
export default connect(mapStateToProps, mapDispatchToProps)(LocationSelect);