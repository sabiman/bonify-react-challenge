import { ActionType, getType } from 'typesafe-actions';

import * as actions from "../store/actions";
import { LOCATION_ADDED, LOCATION_SET_RADIUS, LOCATION_SET_NEW, API_VIDEO_READY, API_VIDEO_ERROR, LOCATION_SET_DIRTY } from '../store/constants';
import { ILocationList, ILocationState } from '../shared/models/interface';

type Action = ActionType<typeof actions>;

const initialState: ILocationState = [];

// Location reducer
export const locationReducer = (state: ILocationState = initialState, action: Action): ILocationState => {
  let list, id;

  switch (action.type) {

    case LOCATION_ADDED:
        return [action.payload].concat(state);

    case LOCATION_SET_RADIUS:
        let radius = action.payload.val;
        id = action.payload.pos;
        if( !isNaN(radius) && radius != state[id].radius) {
            let list = state.slice(0);
            list[id].radius = radius;
            return list;
        }
        return state;

    case LOCATION_SET_NEW:
        return [action.payload.location].concat(state);

    case LOCATION_SET_DIRTY:
        list = state.slice(0);
        list[action.payload].dirty = true;
        return list;

    case API_VIDEO_READY: 
        list = state.slice(0);
        id = action.payload.index;
        list[id].videos = action.payload.videos;
        list[id].fetched = true;
        list[id].dirty = false;
        return list;
    
    case API_VIDEO_ERROR:
        list = state.slice(0);
        id =  action.payload.index;
        delete list[id].videos;
        list[id].fetched = false;
        list[id].dirty = true;
        return list;

    default:
      return state;
  }
};