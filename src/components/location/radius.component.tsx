import * as React from "react";
import {ChangeEventHandler} from 'react';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as actions from "../../store/actions";
import { ILocation, Action } from "../../shared/models/interface";
import { RootState } from "../../reducers";

interface OwnProps {};

export interface IRadiusSelectProps {
    setRadius?: ChangeEventHandler;
    id: number;
    radius: number;
    visible: boolean;
}

// Reverse functions for control value to km conversion
function val2km(val: number): number {
    return ( val + 1 ) * ( val + 1 ) / 10;
}

function km2val(km: number): number {
    return Math.sqrt(km * 10) - 1;
}

// Render search radius selector control
const RadiusSelect: React.SFC<IRadiusSelectProps> = (props: IRadiusSelectProps) => {
    if(!props.visible) {
        return null;
    }
    return (
        <form className="radius-control">
            <div className="form-group">
                <label htmlFor="formControlRange">Search within radius: {props.radius}km</label>
                <input type="range"
                       className="form-control-range"
                       id="formControlRange"
                       value={ km2val(props.radius) }
                       onChange={props.setRadius.bind(props)} />
            </div>
        </form>
    );
}

const mapStateToProps = (state: RootState) : IRadiusSelectProps => {
    let location: ILocation = state.location[state.app.currentLocation];
    let radius = location ? location.radius : 0;
    let id = state.app.currentLocation;

    return { radius, id, visible: radius > 0};
};

const mapDispatchToProps = (dispatch: Dispatch<Action>, props: OwnProps) => bindActionCreators({
    setRadius: function(ev){
        return actions.locationSetRadius(this.id, val2km(parseInt(ev.currentTarget.value)))}
  }, dispatch);

  
export default connect(mapStateToProps, mapDispatchToProps)(RadiusSelect);