import { ActionType, getType } from 'typesafe-actions';

import * as actions from "../store/actions";
import { LOCATION_FORWARD,
        LOCATION_BACKWARD,
        LOCATION_ADDED,
        MAP_PICK,
        LOCATION_SET_NEW,
        API_VIDEO_READY,
        API_VIDEO_ERROR,
        MAP_FETCH_DATA
    } from '../store/constants';
import { IAppState } from './../shared/models/interface';

type Action = ActionType<typeof actions>;

// App initial state
const initialState: IAppState = {
    youTubeLoading: false,
    currentLocation: 0,
    newLocation: true,
    locationLen: 0,
    errorMessage: ''
};

// App state reducer
export const appReducer = ( state: IAppState = initialState, action: Action ): IAppState => {

    let {currentLocation, locationLen} = state;

    switch (action.type) {
        case LOCATION_ADDED: 
            locationLen++;
            return {...state, locationLen, newLocation: true};

        case LOCATION_BACKWARD:
            currentLocation++;
            return currentLocation < locationLen ? {...state, currentLocation, newLocation: false, errorMessage: ''} : state;

        case LOCATION_FORWARD:
            currentLocation--;
            return currentLocation >= 0 ? {...state, currentLocation, newLocation: false, errorMessage: ''} : state;    

        case LOCATION_SET_NEW:
            return {...state, currentLocation: 0, errorMessage: ''};
            
        case API_VIDEO_READY:
            return {...state, youTubeLoading: false, newLocation: true, errorMessage: ''}; 

        case API_VIDEO_ERROR:
            return {...state, youTubeLoading: false, errorMessage: action.payload.error.message};  
            
        case MAP_PICK:
            locationLen++;
            return {...state, locationLen, errorMessage: ''};    
            
        case MAP_FETCH_DATA:
            return {...state, youTubeLoading: true};

        default:
            return state;
    }
};