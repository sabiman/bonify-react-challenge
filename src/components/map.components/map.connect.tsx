import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as actions from "../../store/actions";
import { RootState } from "../../reducers";
import Map from "./map.component";
import { ILocation, Action } from '../../shared/models/interface';

interface OwnProps {}

// Connect Map component to store

const mapStateToProps = (state: RootState) => {
    let location: ILocation = state.location[state.app.currentLocation];
    let radius = location ? location.radius : 50;
    let center = location ? location.center : {lat: 51, lng: 9}; //todo: default position on start from Current location?
    let enabled = location && !state.app.youTubeLoading;
    let dirty = location && location.dirty;
    let fetched = location && location.fetched;
    let newLoc = state.app.newLocation;

    return {radius, center, enabled, dirty, fetched, newLoc};
};

const mapDispatchToProps = (dispatch: Dispatch<Action>, props: OwnProps) => bindActionCreators({
    setLocation: (lat: number, lng: number) => actions.mapPickLocationAction(lat, lng),
    fetchVideos: (pos, radius) => actions.mapFetchDataAction(pos, radius),
    apiReady: () => actions.apiReadyAction(),
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Map);