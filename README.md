# React-redux googleapis sample.

A sample application for React + redux-observable + TypeScript
Uses GoogleMaps, Youtube and GeoCoding API's.

## Demo

https://bonify-challenge.web.app/


## Installing / Getting started
   
```
npm install
npm start 
```

Then open `index.html` in your favorite browser and everything should be ready to use!

## Prerequisites.

Build an application that uses the Google Maps API and Youtube API to grab and
display the latest videos by a location defined on the map. Please add small back/forward
functionality which will allow user to go throw the history of selected map locations in
current session( visually it should be only two buttons “Back” and “Forward” on the screen).

### Assumptions

- Use GeoCoding API to display readable name for picked location
- Add possibility to set the radius of search around selected point
- Create responsive layout

### Confines used in scope of demo MVP applications

- Only 50 first results of the search are presented, no pagination
- UX can be not very sane, since reasons of application are demonstrative
- Layout for mobile devices is draft

## Approach

### Techology

- Application is build using React
- Typescript applied for further type sanity
- Redux is used together with Observables - most possible scenarios of redux application demonstrated
- Bootstrap is used for styling - it is a fast approach for greenfield short life project 
- SASS is used - just for fun
- Application is attached to CI and deployed to Firebase.

### Architecture

- Redux store is separated in 2 main parts - 'app' (representing app state) and 'location' - picked locations and video arrays.
- First approach was not to keep the search results at all (since it should be invalidated then), however, just to make location switch more quick it was decided to keep the results (the color of search area represents the validity of list - grey - means invalid, green - just received, red - not fetched yet, or error)
- Previously saved locations cannot be moved, but the corresponding list can be refreshed and search radius can be edited

## Licensing

MIT
