import * as React from "react";
import { IVideoItem } from "../../shared/models/interface";


export interface IVideoItemProps extends IVideoItem{
    key: number;
}

// Video item presentation component
export const VideoItem: React.SFC<IVideoItemProps> = (video: IVideoItemProps) => {
    return (
        <dl className="video-item bd-highlight">
            <img className="img-thumbnail" src={video.thumbnail.url}></img>
            <dt className="">
                <a href={"http://youtu.be/" + video.videoId} target="_blank">{video.title.toString()}</a>
            </dt>
            <dd className="">{video.description}</dd>
        </dl>
    );
}