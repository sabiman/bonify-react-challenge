import * as React from "react";
import { connect } from 'react-redux';
import { RootState } from "../../reducers";
import { ILocation } from "../../shared/models/interface";

export interface LocationPresentProps extends ILocation {
    id: number;
}

type NumericProp = number | (() => number);

// Trim numeric coordinates to 4 digits after point
const trimLatLng = ( val: NumericProp ) => {
    let numeric = val instanceof Function ? val() : val;
    return numeric.toFixed(4);
}

// Render the name (from GeoCoding API) and coordinates for picked location
const LocationPresent: React.SFC<LocationPresentProps> = ( props: LocationPresentProps ) => {
    if(props.fetched === undefined) {
        return (
            <div className="text-light px-3">
                <h5>Pick a location on the map</h5>
                <span> </span>
            </div>)   
        }
        return (
            <div className="text-light px-3">
                <h5>{props.name}</h5>
                <span className="coord">lat: {trimLatLng(props.center.lat)}</span>
                <span className="coord">len: {trimLatLng(props.center.lng)}</span>
            </div>
        );
}

const mapStateToProps = ( state: RootState ) : LocationPresentProps => {
    let location: ILocation = state.location[state.app.currentLocation];
    let radius = location ? location.radius : 0;
    let center = location ? location.center : {lat: 0, lng: 0} ;
    let name = location ? location.name : '';
    let dirty = location && location.dirty;
    let fetched = location && location.fetched;
    let id = state.app.currentLocation;

    return { radius, center, name, dirty, fetched, id };
};

const mapDispatchToProps = (): {} => {return {}};
 
export default connect(mapStateToProps, mapDispatchToProps)(LocationPresent);