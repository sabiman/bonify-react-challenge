import * as scriptjs from "scriptjs";
import { GOOGLE_API_KEY } from './../../../google.api.keys';

const GOOGLEAPIS_URL = `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API_KEY}`;
const GOOGLE_PLATFORM_URL = `https://apis.google.com/js/platform.js`;
const GOOGLE_CLIENTAPI_URL = `https://apis.google.com/js/api.js`;

// Load Google API Client
const loadClient = (): PromiseLike<void> => {
    return new Promise((resolve, reject) => {
        gapi.load('client', ()=> {
            gapi.client.setApiKey(GOOGLE_API_KEY);
            resolve();
        })
    });
}

export const initGoogleAPI = (): Promise<Array<unknown>> => {
        return Promise.all([
            // Load platform script  
            new Promise(( resolve ) => {
            scriptjs(GOOGLE_PLATFORM_URL, resolve);
        }),
        //Load API script
        new Promise(( resolve ) => {
            scriptjs(GOOGLEAPIS_URL, resolve);
        }),
        //Load Client and script
        new Promise((resolve ) => {
            scriptjs(GOOGLE_CLIENTAPI_URL, ()=> {
            loadClient()
                .then( ()=> { resolve() });
        });
    })])
}