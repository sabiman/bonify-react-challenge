import { Epic } from "redux-observable";
import { of } from 'rxjs';
import { switchMap, filter } from 'rxjs/operators';
import { ActionType, isActionOf } from 'typesafe-actions';

import * as actions from "../actions";
import { RootState } from "../../reducers";

type Action = ActionType<typeof actions>;

// Update dirty status for location index should be retrieved from other part of the store
const locationSetDirtyEpic: Epic<Action, Action, RootState> = ( action$, store ) =>
    action$.pipe(
        filter(isActionOf( actions.locationSetRadius )),
        switchMap(action => of(actions.locationSetDirty( store.value.app.currentLocation )))      
    );

export default [
  locationSetDirtyEpic
];
