import { Epic } from "redux-observable";
import { from, of } from 'rxjs';
import { switchMap, filter, map, catchError, tap } from 'rxjs/operators';
import { ActionType, isActionOf } from 'typesafe-actions';

import * as actions from "../actions";
import { RootState } from "../../reducers";
import { getLocationName } from "../../shared/services/location.name.service";
import { fetchVideos } from "../../shared/services/video.service";
import { IVideoItem } from "../../shared/models/interface";

type Action = ActionType<typeof actions>;

// Map location pick. Get location name, emit new location with this name
const mapPickEpic: Epic<Action, Action, RootState> = (action$, store) =>
    action$.pipe(
        filter(isActionOf(actions.mapPickLocationAction)),
        switchMap(action => {
            return from(getLocationName(action.payload)).pipe(
                map(name => actions.locationSetNewAction({
                        name,
                        center: action.payload,
                        radius: 50,
                        dirty: false,
                        fetched: false
                    }, store.value.location.length + 1))
                )
        })
    );

// Fetch data for location. Handle errors
const mapFetchEpic: Epic<Action, Action, RootState> = (action$, store) =>
    action$.pipe(
        filter(isActionOf(actions.mapFetchDataAction)),
        switchMap(action => {
            return from(fetchVideos(action.payload.location, action.payload.radius)).pipe(
                map(res => {
                    if( typeof res === 'string' ) {
                        return actions.apiVideoErrorAction(new Error(res), store.value.app.currentLocation);
                    } else {
                        return actions.apiVideoReadyAction(res as IVideoItem[], store.value.app.currentLocation);
                    }
                }
            )
        )}),
        catchError(error => of(actions.apiVideoErrorAction(error, store.value.app.currentLocation)))
    );

export default [
        mapFetchEpic,
        mapPickEpic
    ];
