import {GeoLocation} from '../models/interface';

export const getLocationName = ( location: GeoLocation ): Promise<string>   => {
    return new Promise(( resolve, reject ) => {
        new google.maps.Geocoder().geocode( {location}, ( res, status ) => {
            if(status === google.maps.GeocoderStatus.OK && res.length > 0) {
                resolve(res[0].formatted_address);
            }
            else {
                const fallbackMsg = `Unnamed location, lat: ${location.lat}, lon: ${location.lng}`
                resolve(fallbackMsg);
            }
        });
    });
};
      