type CircleCenter = google.maps.LatLng | google.maps.LatLngLiteral;
type Map = google.maps.Map

const DEF_RADIUS = 50000;
const DEF_STROKE_COLOR = '#FF0000';
const DEF_FILL_COLOR = '#FF0000';
const DEF_FILL_OPACITY = 0.35;
const DEF_STROKE_WEIGHT = 2;
const DEF_STROKE_OPACITY = 0.8;

interface IMapCircle extends google.maps.Circle {};

// Circle fabric
export default function( map: Map, center: CircleCenter, radius?: number ): IMapCircle {
    
    class MapCircle extends google.maps.Circle {

        constructor(map: Map, center: CircleCenter, radius: number = DEF_RADIUS) {
            super({
                map,
                center,
                radius,
                strokeColor: DEF_STROKE_COLOR,
                strokeWeight: DEF_STROKE_WEIGHT,
                strokeOpacity: DEF_STROKE_OPACITY,
                fillColor: DEF_FILL_COLOR,
                fillOpacity: DEF_FILL_OPACITY,
            });
        }
    }

    return new MapCircle(map, center, radius);
}