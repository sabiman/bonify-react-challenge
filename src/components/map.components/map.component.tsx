import * as React from "react";
import circleFabric from './map.circle.class';
import createMap from './map.core.class'
import { GeoLocation } from "../../shared/models/interface";
import { initGoogleAPI } from "../../shared/services/api.init";

export interface MapProps {
  setLocation: (lat: number, lng: number) => undefined;
  fetchVideos: (center: GeoLocation, radius: number) => undefined;
  apiReady: Function;
  enabled: boolean;
  dirty: boolean;
  newLoc: boolean;
  fetched: boolean;
  radius: number;
  center: GeoLocation
}

// Map component
export default class Map extends React.Component<MapProps> {

    private map: google.maps.Map;
    private circle: google.maps.Circle

    constructor(props: MapProps) {
        super(props);
        this.onLoaded = this.onLoaded.bind(this);
    }


    public componentWillMount() {
        initGoogleAPI()
            .then(this.onLoaded);
    }

    fetchData() {
        if(this.props.enabled) {
            this.props.fetchVideos(this.props.center, this.props.radius);
        }    
    }

    public render() {

        // Init circle object, if map is ready
        if( this.map && !this.circle ) {
            this.circle = circleFabric(this.map, this.props.center);

            // Add data fetch listener
            this.circle.addListener('click', this.fetchData.bind(this));
        }

        // Set circle parameters, if it exists and recenter map
        if( this.circle ) {
            let fillColor: string; 
            this.circle.setVisible(this.props.dirty!== undefined);
            this.circle.setRadius(this.props.radius * 1000);
            this.circle.setCenter(this.props.center);

            if(this.props.dirty) {
                fillColor = 'red';
            } else if(this.props.fetched) {
                fillColor = this.props.newLoc? 'green': 'grey';
            } else {
                fillColor = this.props.fetched? 'grey' : 'red';
            }
            this.circle.setOptions({fillColor});

            // TODO: make a better zooming formula
            let zoomFactor = Math.ceil( Math.log2( 200000 / this.props.radius ) / 2 ) + 1;

            this.map.setZoom(zoomFactor);
            this.map.setCenter(this.props.center);
        }

        // render container
        return (
            <div id="map" className="map" />
        );
    }

    private onLoaded() {
        // Create map object
        this.map = createMap(document.getElementById("map"), { lat: 0, lng: 0 });
    
        // Add location pick listener
        this.map.addListener("click", ( event ) => {
            this.props.setLocation(
                event.latLng.lat(),
                event.latLng.lng(),
            );
        });

        this.props.apiReady();
    }
}
