import { API_READY } from '../store/constants';
import { IMapState, Action } from '../shared/models/interface';

const initialState = {
  ready: false
};

// Map reducer
export const mapReducer = ( state: IMapState = initialState, action: Action ): IMapState => {

  switch (action.type) {

    case API_READY:
      return Object.assign({}, state, { ready: true });

    default:
      return state;
  }
};
