import { IVideoItem, IVideoThumbnail } from '../models/interface';
import { GeoLocation } from '../models/interface';

const YOUTUBE_API_PATH = '/youtube/v3/search';
const YOUTUBE_API_METHOD = 'GET';
const REQ_PART = 'snippet';
const RESP_ORDER = 'date';
const RESP_TYPE = 'video,list';
const RESP_FILLER = 'n/a';
const GEN_ERROR_MSG = 'unknown error';
const MAX_RESULTS = 50;

// Make sure the client is loaded and sign-in is complete before calling this method.
export const fetchVideos = (pos: GeoLocation, radius: number): PromiseLike<IVideoItem[] | string> => 
    gapi.client.request({ method: YOUTUBE_API_METHOD,
            path: YOUTUBE_API_PATH,
            params: {
                part: REQ_PART,
                location: `${pos.lat}, ${pos.lng}`,
                locationRadius: `${radius}km`,
                maxResults: MAX_RESULTS,
                order: RESP_ORDER,
                type: RESP_TYPE          
            }
        })
        .then((response: any) => {
        // Handle the results here (response.result has the parsed body).
            if(response.result && response.result.items) {
                return response.result.items.map((item: any) => {
                    let snippet = item.snippet;
                    let id = item.id;
                    let thumbnail: IVideoThumbnail = item.snippet.thumbnails.default;
                    return {
                        videoId: id.videoId || RESP_FILLER,
                        title: snippet.title || RESP_FILLER,
                        description: snippet.description || RESP_FILLER,
                        channelTitle: snippet.channelTitle || RESP_FILLER,
                        thumbnail
                    }
                })
            } else {
                return [];
            }
        }, (error: any) => {
                // Handle error
                if(error && error.result && error.result.error && error.result.error.message) {
                    return error.result.error.message;
                }
                return GEN_ERROR_MSG;
        })

        