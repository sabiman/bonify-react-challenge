import * as React from "react";
import { Dispatch } from 'redux';
import { connect } from 'react-redux';

import * as actions from "../../store/actions";
import { RootState } from "../../reducers";
import { IVideoItem, Action } from "../../shared/models/interface";
import { VideoItem } from "./video.item.component";

interface OwnProps {};

export interface IVideoListProps {
  videos?: IVideoItem[],
  loading: boolean;
  errorMessage?: string;
};

// Video list component
const VideoList: React.SFC<IVideoListProps> = ({ videos, loading, errorMessage }) => {
    // Render error
    if( errorMessage ) {
        return (
            <React.Fragment>
                <h6 className="text-danger">Error. loading videos.</h6>
                <span>{errorMessage}</span>
            </React.Fragment>
        );
      }
      // Render loader
      if( loading ) {
          return (
              <React.Fragment>
                  <h6>Busy. loading videos...</h6>
                  <div className="spinner-grow" role="status">
                      <span className="sr-only">Busy, loading videos...</span>
                  </div>
              </React.Fragment>
            )
      }
      // Render empty list
      if (!videos) {
          return (
              <div>
                  <h6>No videos fetched yet</h6> 
                  <span>(click the circle on the map)...</span>
              </div>
          );
      }
      // Render list
      return (
          <div className="video-wrapper d-flex flex-row bd-highlight mb-3">
              {videos.map((video, i) => (
                      <VideoItem {...video} key={i}></VideoItem>
                  ))
              }
          </div>
      )
};

const mapStateToProps = (state: RootState) => {
    let id = state.app.currentLocation;
    let location = state.location[id];
    let videos = location && location.videos; 
    let loading = state.app.youTubeLoading;
    let errorMessage = state.app.errorMessage;

    return {
        videos, loading, errorMessage
    }
};

const mapDispatchToProps = (dispatch: Dispatch<Action>, props: OwnProps) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(VideoList);
