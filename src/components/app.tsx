import './../style/style.sass';
import * as React from "react";

import LocationSelect from "./location/location.select.component";
import LocationPresent from "./location/location.present.component";
import VideolistComponent from "./video/videolist.component";
import Map from "./map.components/map.connect";
import RadiusSelect  from "./location/radius.component";

export interface IAppProps {}

export const App: React.SFC<IAppProps> = (props: IAppProps) =>
        (
            <React.Fragment>
                <nav className="navbar navbar-dark
                        fixed-top bg-dark
                        flex-md-nowrap
                        p-0 shadow dash-nav">
                    <LocationPresent />
                    <ul className="navbar-nav px-3">
                        <li className="nav-item text-nowrap">
                            <LocationSelect />
                        </li>
                    </ul>
                </nav>
                <div className="map-wrapper row justify-content-md-left">
                    <div className="map-fixed col-md-auto">
                        <RadiusSelect />
                        <Map />
                    </div>  
                    <div className="videos-container col-lg-3">
                        <h4 className="h4">Videos</h4>
                        <VideolistComponent />
                    </div>
                </div>
                <div className="footer">(c) Gleb</div>
            </React.Fragment>
        );