type Map = google.maps.Map;
type MapCenter = google.maps.LatLng | google.maps.LatLngLiteral;
type MapOptions = google.maps.MapOptions;

const DEF_ZOOM = 8;
const DEF_CTRLS = {
    MAP_TYPE: false,
    FULL_SCREEN: false,
    KEYB_SHORTCUTS: false,
    STREET_VIEW: false,
    DISABLE_DCLCK_ZOOM: false,
    SCALE: false,
    ROTATE: false,
    PAN: false
};

export interface IMapCore extends Map {};  

// Map fabric
export default function( container: Element, center: MapCenter ): IMapCore {
    class MapCore extends google.maps.Map {
        constructor(container: Element, center: MapCenter) {
            super( container, {
                center,
                zoom: DEF_ZOOM,
                mapTypeControl: DEF_CTRLS.MAP_TYPE,
                disableDoubleClickZoom: DEF_CTRLS.DISABLE_DCLCK_ZOOM,
                fullscreenControl: DEF_CTRLS.FULL_SCREEN,
                keyboardShortcuts: DEF_CTRLS.KEYB_SHORTCUTS,
                streetViewControl: DEF_CTRLS.STREET_VIEW,
                scaleControl: DEF_CTRLS.SCALE,
                rotateControl: DEF_CTRLS.ROTATE,
                panControl: DEF_CTRLS.PAN,
            });
        }
    }

    return new MapCore(container, center);
} 