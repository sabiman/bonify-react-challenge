// API actions
export const API_READY   = "@@api/READY";
export const API_VIDEO_READY   = "@@api/VIDEO_READY";
export const API_VIDEO_ERROR   = "@@api/VIDEO_ERROR";

// Map related actions
export const MAP_PICK = "@@map/PICK";
export const MAP_FETCH_DATA = "@@map/FETCH_DATA";

// Location manipulation related actions
export const LOCATION_FORWARD = '@@location/FW';
export const LOCATION_BACKWARD = '@@location/BACK';
export const LOCATION_ADDED = '@@location/ADDED';
export const LOCATION_SAVE = '@@location/SAVE';
export const LOCATION_SET_RADIUS = '@@location/SET_RADIUS';
export const LOCATION_SET_DIRTY = '@@location/SET_DIRTY';
export const LOCATION_SET_NEW = '@@location/SET_NEW';
