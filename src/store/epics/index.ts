import { combineEpics } from "redux-observable";

import locationEpic from "./location.epic";
import mapEpic from "./map.epic";

const epics = combineEpics(
  ...locationEpic,
  ...mapEpic
);

export default epics;
