import { ActionType } from 'typesafe-actions';
import * as actions from "../../store/actions";

export type Action = ActionType<typeof actions>;

export type GeoLocation = google.maps.LatLng | google.maps.LatLngLiteral;

export interface IVideoThumbnail {
    url: string;
    width: number;
    height: number;
}

export interface IVideoItem {
    videoId: string,
    publishedAt: string
    title: string;
    description: string;
    channelTitle: string;
    thumbnail: IVideoThumbnail
}

export interface ILocation {
    center: GeoLocation;
    radius: number;
    name: string;
    dirty: boolean;
    fetched: boolean;
    videos?: IVideoItem[]
}

export interface ILocationList extends Array<ILocation> {}

export interface IMapState {
    readonly ready: boolean;
}

export interface IAppState {
    youTubeLoading: boolean;
    currentLocation: number;
    locationLen: number;
    newLocation: boolean;
    errorMessage: string;
}

export interface ILocationState extends ILocationList {}
