import { createAction } from "typesafe-actions";
import { ILocation, IVideoItem, GeoLocation } from "../../shared/models/interface";

import {
  API_READY,
  LOCATION_BACKWARD,
  LOCATION_FORWARD,
  LOCATION_ADDED,
  LOCATION_SAVE,
  LOCATION_SET_RADIUS,
  LOCATION_SET_DIRTY,
  MAP_PICK,
  LOCATION_SET_NEW,
  MAP_FETCH_DATA,
  API_VIDEO_READY,
  API_VIDEO_ERROR
} from "../constants";

export const locationFFWDAction = createAction(LOCATION_FORWARD);
export const locationBACKAction = createAction(LOCATION_BACKWARD);
export const locationSaveAction = createAction(LOCATION_SAVE);
export const locationAddedAction = createAction(LOCATION_ADDED, resolve => (loc: ILocation) => resolve(loc));
export const locationSetDirty = createAction(LOCATION_SET_DIRTY, resolve => (pos: number) => resolve(pos));
export const locationSetRadius = createAction(LOCATION_SET_RADIUS, resolve => (pos: number, val: number) => resolve({pos, val}));
export const locationSetNewAction = createAction(LOCATION_SET_NEW, resolve => (location: ILocation, len: number) => resolve({location, len}));

export const mapFetchDataAction = createAction(MAP_FETCH_DATA, resolve => (location: GeoLocation, radius: number) => resolve({location, radius}));
export const mapPickLocationAction = createAction(MAP_PICK, resolve => (lat: number, lng: number) => resolve({ lat, lng }));

export const apiVideoReadyAction = createAction(API_VIDEO_READY, resolve => (videos: IVideoItem[], index: number) => resolve({videos, index}));
export const apiVideoErrorAction = createAction(API_VIDEO_ERROR, resolve => (error: Error, index: number) => resolve({error, index}));
export const apiReadyAction = createAction(API_READY);



