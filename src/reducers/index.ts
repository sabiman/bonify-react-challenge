import { combineReducers } from "redux";

import { mapReducer } from "./map.reducer";
import { locationReducer } from './location.reducer';
import { appReducer } from './app.reducer'
import { IAppState, ILocationState, IMapState } from "../shared/models/interface";


export type RootState = {
  map: IMapState;
  location: ILocationState;
  app: IAppState
};

const reducers = combineReducers({
  map: mapReducer,
  location: locationReducer,
  app: appReducer
});

export default reducers;
